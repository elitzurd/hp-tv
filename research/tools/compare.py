#!/usr/bin/python3

import csv
import os
import dominate
from dominate import tags


#os.system("""ack "<[a-z][^>]*>" ../welsh/english.xml -o | sed 's/<[a-z]* id="\([0-9]*\)" type="\([^"]*\)" speaker="\([^"]*\)" addressee="\([^"]*\)".*/\1\t\2\t\3\t\4/g' > /tmp/a""")
os.system('./maketable.sh mandarin > /tmp/b')

def get_language_data(language):
    os.system(f'./maketable.sh {language} > /tmp/a')
    foo = {}
    with open('/tmp/a') as csvdata:
        csvreader = csv.DictReader(csvdata, delimiter='\t',
                fieldnames=('pronoun', 'id', 'type', 'speaker', 'addressee'))
        for row in csvreader:
            foo[row['pronoun'], row['id']] = {
                    'type': row['type'],
                    'speaker': row['speaker'],
                    'addressee': row['addressee']
                    }
    return foo

langa = get_language_data('welsh')
langb = get_language_data('mandarin')



T = ['ti', 'ni']
V = ['chi-sg', 'nin']

def make_table():
    with tags.table() as t:
        for row in langa:
            if langb[row]['type']:
                rowcolour = ''
                if ((langa[row]['type'] in T
                    and langb[row]['type'] in T) or
                    (langa[row]['type'] in V
                        and langb[row]['type'] in V)):
                        rowcolour = 'match'
                elif ((langa[row]['type'] in T
                    and langb[row]['type'] in V) or
                    (langa[row]['type'] in V
                        and langb[row]['type'] in T)):
                        rowcolour = 'mismatch'
                with tags.tr(cls=rowcolour):
                    tags.td(row[0])
                    tags.td(row[1])
                    tags.td(langa[row]['type'])
                    tags.td(langb[row]['type'])
                    tags.td(langa[row]['speaker'])
                    tags.td(langa[row]['addressee'])
    return t

with tags.html() as output:
    with tags.head():
        tags.meta(charset='utf-8')
        tags.style("""
            td {padding: 0.3em}
            .match {background-color: lightgreen}
            .mismatch {background-color: pink}""")
    with tags.body():
        make_table()

print(output)

#ack "<[a-z][^>]*>" english.xml -o |\
#	sed 's/<[a-z]* id="\([0-9]*\)" type="\([^"]*\)" speaker="\([^"]*\)" addressee="\([^"]*\)".*/\1\t\2\t\3\t\4/g'
