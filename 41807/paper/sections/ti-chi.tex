\section{\C{ti:chi}}

\noindent\partitle{Welsh T-V forms}%
Similarly to many European languages, Welsh has a politeness distinction in only in the \grammar{2sg}, the V-form being homonymic with \grammar{2pl}. The T-form is \C{ti} and the V-form is \C{chi}.\footnote{Their etymology is from their corresponding PIE pronouns: \C{ti}~< Middle Welsh \C{ti}~< Proto-Brythonic *\textit{ti}~< Proto-Celtic *\textit{tū}~< *\textit{túh₂} (\grammar{2sg.nom}) and \C{chi}~< \C{chwi}~< *\textit{hwi}~< *\textit{swīs} (cf.\ Gaulish *\textit{suis})~< *\textit{wos}, *\textit{wēs} (\grammar{2pl.obl}).}

$$
\mbox{\ruby{\E{you-}}{\grammar{2}} \rightdashedarrow\enspace}
\begin{dcases}
	\crotatebox{90}{\textsc{personal}}
	\begin{dcases}
		\mbox{\ \quad\ruby{\textbf{\C{ti}}}{\grammar{2sg.fam}}}\\
		\mbox{\ \quad\mbox{\C{chi}}\enspace}
		\begin{dcases}
			\mbox{\begin{minipage}{3.75em}\centering
					\quad\ruby{\textbf{\C{chi}}}{\grammar{2sg.hon}}\\~\\
					\quad\ruby{\C{chi}}{\grammar{2pl}}
				\end{minipage}
			}
		\end{dcases}
	\end{dcases}\\
	%\mbox{\quad\ruby{\HLchdi{chdi}}{2sg.fam.?}}\\
	\mbox{\textsc{(non-personal)}}
\end{dcases}
$$

\partitle{Idiomatics}%
When translating impersonal\footcite[See][]{helmbrecht.j:2015:non-prototypical} or ‘generic’ occurrences of Eng.\ \E{you-} the translator usually used a Welsh second person pronoun, reflecting the sociopragmatic interpersonal relation between the speaker and the addressee just like in actual, referential usage. The other, less commonly-used strategy was to use non-personal constructions, as in \cref{ex:angen,ex:rhywun}. In addition, some occurrences of \E{you-} in more-or-less bound phrases like \E{tell}\symbolglyph{‿}\E{you} or \E{thank}\symbolglyph{‿}\E{you} are not corresponded in the translation with a pronoun, according to the idiomatic valency use in Welsh (e.g.\ \cref{ex:tellyou}).

\partitle{Loci of the T-V distinction}%
The loci of the T-V distinction are in independent as well as dependent pronouns, including the verbal system. One locus which does not correspond to English \E{you-} is the imperative, which is generally marked by a zero pronoun in English.

\partitle{Procedure}\label{sec:procedure}%
I’ve manually checked the way all occurrences of \E{you-} were translated, tagging them with ‘speaker’, ‘addressee’ and \C{‘ti:chi’} information (viz.\ who speaks to whom how).\footnote{1393 occurrences in total~—
\E{you:} 1034,
\E{your:} 148,
\E{*yeh:} 121,
\E{*yer:} 60,
\E{yourself:} 14,
\E{yours:} 8,
\E{yourselves:} 5,
\E{*yerself:} 2,
\E{*yerselves:} 1. *=West Country dialectal forms, used by the character Hagrid.} By using an automated script that extracts the data and outputs it in a usable form, an intricate map of sociopragmatic relationships emerges from these tags. Using this map, and three topics are to be addressed in this paper:

\begin{compactitem}
	\item \partitle{Structure}%
		change in address form (\cref{sec:change}),
	\item addressing someone unidentified (such as someone knocking on the door) or non-specific (\cref{sec:unknown}),
	\item and the relationship between children and grown-ups (\cref{sec:age}).
\end{compactitem}



\subsection{Change in address form}\label{sec:change}

\partitle{Change in address form}%
The relationships between characters are not set in stone; there are some cases in which transitioning from one address form to the other one such a change.

\partitle{Harry~→ Hagrid}%
For example, when eleven years old Harry first meets Hagrid he is a stranger, a grown-up man whom he doesn’t know, so he speaks to him using the formal \C{chi} (\cref{ex:Harry-Hagrid-A}\footnote{A single underline in the examples appendix is used for \C{ti}; a double one for \C{chi}.}), but as Hagrid tells his story and the close connection between them is revealed, Harry transitions to using \C{ti} (\cref{ex:Harry-Hagrid-B}), which he continues to do throughout the book.

\partitle{Hermione~→ Hagrid}%
Hagrid is a very interesting character sociopragmatically, standing somewhere between the teachers and the students. Thus it is not surprising that as the special mixed-age clique of Harry, Hermione, Ron and Hagrid develops over time Hermione starts using \C{ti} towards Hagrid as well (\cref{ex:Hermione-Hagrid-A,ex:Hermione-Hagrid-B}).% \optional{In this particular example the use of \C{ti} might also act as a rhetorical device of flattering, signalling closeness.}

\partitle{Vernon~→ Hagrid}%
With Vernon, Harry’s adoptive father, the analogous transition when speaking to Hagrid bears a completely different meaning: from politeness (even when demanding Hagrid to leave; \cref{ex:Vernon-Hagrid-A}) to direct, straightforward harsh speech (\cref{ex:Vernon-Hagrid-B}) the moment Hagrid is going to tell Harry the secret Vernon fears the most: that Harry is a wizard and his parents, who were wizards as well, were killed magically.

\partitle{Firenze~→ Harry}%
\Cref{ex:Firenze-Harry} is of a different kind: Firenze the centaur speaks to what was for him at the moment a random student from the nearby school for magic with \C{ti}, but when he realizes it is actually Harry he transitions to \C{chi}, showing him respect (cf.\ \cref{sec:chi-ing Harry}). This is not a development in relationship, but a change caused by not recognizing the addressee correctly at first.



\subsection{Unidentified or non-specific addressee}\label{sec:unknown}

\partitle{Unidentified or non-specific addressee}%
The \C{ti:chi} opposition is grounded in the concrete pragmatic situation. When one does not know with whom they speak, \C{chi} is used exclusively, \C{ti} being too loaded, too specific for such a situation. This happens in the text when the addressee is hidden or when addressing a non-specific reader:

\begin{compactitem}
	\item Unidentified addressees: \cref{ex:hidden door} (someone behind a door); \cref{ex:hidden forest} (something moving in the Forbidden Forest, only to be revealed as Ronan afterwards, whom Hagrid greets with \C{ti}); \cref{ex:hidden cloak} (Harry, Hermione and Ron hidden beneath the Invisibility Cloak).
	\item Non-specific readers:
		\begin{compactitem}
		\item By in-book characters: \cref{ex:reader book titles} (in book titles); \cref{ex:reader puzzle} (in a written puzzle).
		\item Literary techniques: \cref{ex:author-readers} (the author uses an impersonal second person, as if she addresses the readers); \cref{ex:FIS} (Free Indirect Speech)\footnote{This example is interesting, because if one turns the pages back to when ‘Gringotts is the safest place etc.’ was actually said, it was Hagrid speaking familiarly with Harry (\cref{ex:re-FIS}). Thus, the use of \C{chi} here does not mirror the original wording but the norm of this literary technique.}.
		\end{compactitem}
\end{compactitem}


\subsection{Age and status}\label{sec:age}

\partitle{Age and status}%
Belonging to the boarding school story genre, the corpus offers an opportunity to examine the linguistic expression of social relationships in which age plays a major role.


\subsubsection{Students and teachers}

%\begin{frame}<1>[shrink, label=students-teachers]{Students \AfromtoB teachers}
\begin{table}
	\centering
	\begin{tabular}{rlll}
		\toprule
        & speaker     & addressee                       & T-V                    \\
		\midrule
        & student     & teacher                         & \C{chi}                \\
        & student     & student                         & \C{ti}                 \\
		\ldelim\{{5}{*}[\crotatebox{90}{T{\toarrow}T}]
        & Dumbledore  & McGonagall                      & \C{ti}                 \\
        & McGonagall  & Dumbledore                      & \C{ti}                 \\
        & Quirrell    & Dumbledore                      & \C{chi}                \\
        & Quirrell    & Snape                           & \C{ti}                 \\
        & Snape       & Quirrell                        & \C{ti}                 \\
		\ldelim\{{7}{*}[\crotatebox{90}{T{\toarrow}S}]
        & Dumbledore  & Harry                           & \C{ti}                 \\
        & Madam Hooch & Neville                         & \C{ti}                 \\
        & McGonagall  & \Stack{Draco, Harry, Hermione, \\ Jordan, Neville, Wood}  & \C{chi} \\
        & Quirrell    & Harry                           & \C{chi}                \\
        & Snape       & Harry, Hermione                 & \C{chi}                \\
        & Snape       & Neville                         & \C{ti}\\
		\bottomrule
	\end{tabular}
	\caption{Students \fromto\ Teachers}\label{students=teachers}
\end{table}

\partitle{S~→~S, S~→~T, T~→~T}%
\Cref{students=teachers} summarizes interactions between students and teachers. As expected, students address teachers with \C{chi} and each other with \C{ti}, equal staff members address each other with \C{ti} (McGonagall and Dumbledore are Deputy Headmistress and Headmaster, respectively; Snape and Quirrell are both teachers), and Quirrell addresses Dumbledore, his superior, using \C{chi}.

\partitle{T~→~S}%
What might seem \foreign{a priori} surprising is the fact teachers use \C{chi} toward students, who rank lower in the school hierarchy. According to \textcite[§4.130]{thomas.p:2006:gramadeg} there was a diachronic change regarding this: at least in the 1960s students and teachers used \C{chi} reciprocally, and this was changed later so that an irreciprocal relationship is now more usual. Therefore, it seems teachers at Hogwarts School of Witchcraft and Wizardry use the older way of addressing. Two reasons may contribute to this choice by the translator: one is that this is what the translator, who was born in 1942, is used to from her days at school; the other is that Hogwarts, being an old-fashioned school, is linguistically presented as such. It might be noted that non-teacher staff usually use \C{ti} towards students.

\partitle{Kindness~≠ politeness}%
\Cref{ex:ylw} is from another corpus \parencite{roberts.k:1960:lon-wen}, and as the situation depicted in it took place at the beginning of last century, it agrees with \posscite[§4.130]{thomas.p:2006:gramadeg} older, reciprocal, system. It is quite an extreme example: headmaster addressing a student using \C{chi} just before caning her. Thus, kindness and politeness are two different things, on two different planes. This can help one understand why Quirrell the Defence Against the Dark Arts teacher, continues to use \C{chi} with Harry even after being revealed as a villain (\cref{ex:Quirrell-Harry-B}). In the same situation Voldemort, the antagonist, addresses Harry with \C{ti} (\cref{ex:Voldemort-Harry}), but he is not Harry’s teacher (and do not wish to show him any respect).

\partitle{Situations in which T~→~S~=~\C{chi}}
There are a few cases in which teachers do address students using \C{ti}. Neville is addressed with \C{ti} by Madam Hooch the Flying teacher (\cref{ex:Hooch-Neville}) and Professor Snape the Potions teacher (\cref{ex:Snape-Neville}) in two situations: in both cases this happens just after he acted very clumsily, which resulted in a broken wrist in one case and a mayhem in class in the other. Therefore, it is understandable why these teachers did not address him using \C{chi} in these particular situations. The exclusive and systematic (more than sixty occurrences) use of \C{ti} by Dumbledore the Headmaster toward Harry is less clear. This is the same address form he uses with the staff members Hagrid and McGonagall. In all three situations\footnote{On Harry’s way to the Mirror of Erised; after a Quidditch game; in the hospital wing.} in which Dumbledore talks to Harry they speak alone~— or quietly enough so that only Harry can hear~— and these are not class or a class-like situations. My hypothesis is that the use of \C{ti} by Dumbledore signals special closeness, not considering Harry as ‘just another Hogwarts students’ to be addressed by the distancing \C{chi}.



\subsubsection{Children and their (adoptive) parents}

\partitle{Families and being excluded from a family}%
\Textcite[§4.130]{thomas.p:2006:gramadeg} states there is a strong tendency to use \C{ti} reciprocally within the family in the current generation. This is the case in the Weasley family (\cref{ex:Fred=Molly}). The relationship between the Dursleys and their adopted child Harry, on the other hand, is not a loving one. This is reflected by the irreciprocal use of second person (\cref{ex:Harry-Vernon,ex:Dursleys-Harry}), as opposed to the reciprocal one with their biological son Dudley (\cref{ex:Dudlay-Vernon}).


\subsubsection{Grown-ups \C{chi}-ing Harry}
\label{sec:chi-ing Harry}

\partitle{Showing respect}%
The first part of the book revolves around one main theme: how Harry the ‘ugly duckling’ turns up to be an admired wizard, ‘a beautiful swan’. The respect people show Harry, who defeated the dreaded Voldemort, is expressed by linguistic means (cf.\ \cref{ex:Firenze-Harry} in \cref{sec:change}). This is most pronounced in the Leaky Cauldron pub, where strangers are delighted to see Harry (\cref{ex:Leaky Cauldron}). Later on Harry and Hagrid’s visit to Diagon Alley they meet Ollivander the wand-maker, who speaks to Harry in \C{chi} (\cref{ex:Ollivander-Harry}), which stands in contrast with his use of \C{ti} towards Hagrid (\cref{ex:Ollivander-Hagrid}).\footnote{\C{Chi} is not socially obligatory in seller~\toarrow\ buyer relationships; see \cref{ex:Malkin-Harry}.}
