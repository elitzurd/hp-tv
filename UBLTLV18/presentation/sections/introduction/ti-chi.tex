\section*{ti:chi}

\begin{frame}<1-2>{T-V distinction: relevant sociolinguistic concepts}
	\begin{multicols}{2}
		\begin{itemize}
			\item age
			\item courtesy
			\item familiarity
			\item gender
			\item honorificity
			\item insult
			\item politeness
			\item<alert@2> power
			\item respect
			\item situation
			\item social distance
			\item<alert@2> solidarity
			\item status
			\item …
		\end{itemize}
	\end{multicols}

	\vfill

	The ‘T’ and ‘V’ terms were coined by \textcite{brown.r+:1960:power-solidarity} after Latin {\emph{tū}} ({\gram{2.sg}}) and {\emph{vōs}} ({\gram{2.pl}}).
\end{frame}


\begin{frame}<1-2>{T-V distinction in European languages}
	\begin{tabular}{llll}
                         & 2\gram{sg.fam} (T)              & 2\gram{sg.hon} (V)               & homonymy\\
		\midrule
		Early Mod.\ Eng. & \foreign{thou}                  & \foreign{ye}                     & 2\gram{pl}\\
		Yiddish          & \Hebrew{דו}\enspace\foreign{du} & \Hebrew{איר}\enspace\foreign{ir} & 2\gram{pl}\\
		French           & \foreign{tu}                    & \foreign{vous}                   & 2\gram{pl}\\
		German           & \foreign{du}                    & \foreign{Sie}                    & 3\gram{pl}~+ capitalizing\\
		French           & \foreign{tu}                    & \foreign{vous}                   & 2\gram{pl}\\
		Russian          & ты\enspace\foreign{ty}          & вы\enspace\foreign{vy}           & 2\gram{pl}\\
		Estonian         & \foreign{sina}                  & \foreign{teie}                   & 2\gram{pl}\\
		Turkish          & \foreign{sen}                   & \foreign{siz}                    & 2\gram{pl}\\
		\alert<2>{Welsh}            & \C{ti}                          & \C{chi}                          & 2\gram{pl}
		%\only<2-3>{Welsh} & \only<2>{\Chinese{太}\enspace\foreign{tai}}\only<3>{ti} & \only<2>{\Chinese{極}\enspace\foreign{chi}}\only<3>{chi} & \only<3>{2\gram{pl}}\\
		%& \onslide<0>{\Chinese{太}\enspace\foreign{tai}} & \onslide<0>{\Chinese{極}\enspace\foreign{chi}} &
	\end{tabular}
\end{frame}


\begin{frame}{Etymology}
	\begin{tabular}{lll}
		Modern Welsh          & \C{ti}         & \C{chi}\\
		\midrule
		< Middle Welsh        & \C{ti}         & \C{chwi}\\
		< Proto-Brythonic     & *\textit{ti}   & *\textit{hwi}\\
		< Proto-Celtic        & *\textit{tū}   & \Stack{*\textit{swīs}\\{\small (cf.\ Gaulish *\textit{suis})}}\\
		< Proto-Indo-European & *\textit{túh₂} & *\textit{wos}, *\textit{wēs}\\
                              & 2\gram{sg.nom} & 2\gram{pl.obl}
	\end{tabular}

	\vfill

	Northern colloquial \C{chdi} /χtiː/{\enspace}<{\enspace}\C{(â) chdi}{\enspace}\gl{<}{dissimilation}{\enspace}\C{(â) th’di}{\enspace}<{\enspace}\C{(â) thydi} ‘with you (reduplicated)’ (see \cite{willis.d:2013:bilbao})
\end{frame}


\begin{frame}{Translation choices}
	\begin{center}
		$$
		\mbox{\ruby{\HLyou{you}}{\gram{2}} \rightdashedarrow\enspace}
		\begin{dcases}
			\crotatebox{90}{\textsc{personal}}
			\begin{dcases}
				\mbox{\quad\ruby{\HLti{ti}}{\gram{2sg.fam}}}\\
				\mbox{\quad\mbox{chi}\enspace}
				\begin{dcases}
					\mbox{\begin{minipage}{3.75em}\centering
							\quad\ruby{\HLchisg{chi}}{\gram{2sg.hon}}\\
							\quad\ruby{\HLchipl{chi}}{\gram{2pl}}
						\end{minipage}	
					}
				\end{dcases}
			\end{dcases}\\
			%\mbox{\quad\ruby{\HLchdi{chdi}}{2sg.fam.?}}\\
			\mbox{\textsc{(non-personal)}}
		\end{dcases}
		$$
	\end{center}

	\vfill

	Theoretical background: \cite{levy.j:1967:translation}.
\end{frame}


\begin{frame}{Dispersing light {\normalfont\symbolglyph{≈}} dispersing language}
	\begin{center}
		\fbox{\includegraphics[width=\textwidth*3/4]{media/newton.jpg}}

		\source{Illustration by Jean-Leon Huens, National Geographic Stock}
	\end{center}
\end{frame}


\begin{frame}{Loci of T-V distinction}
	\begin{tabular}{rlll}
		Eng.\ eq. & locus               & \HLti{ti}       & \HLchisg{chi}\\
		\midrule
		~\hfill\ldelim\{{9}{*}[\crotatebox{90}{\hlyou{you, …}}]
                  & simple, weak        & \C{ti}             & \C{chi}\\
                  & simple, strong      & (\C{ti}), \C{chdi} & \C{chi}\\
                  & reduplicated        & \C{(tydi)}         & \C{(chwychwi)}\\
                  & conjunctive         & \C{tithau}         & \C{chithau}\\
                  & possessive (indep.) & \C{dy}             & \C{eich}\\
                  & possessive (dep.)   & \C{’th}            & \C{’ch}\\
                  & prepositions        & \C{arnat}          & \C{arnoch}\\
                  & ‘\C{bod}’ forms     & \C{(yd)wyt}        & \C{(yd)ych}\\
                  & finite verbs        & \C{canaist, …}     & \C{canasoch, …}\\
		~\hfill\ldelim\{{1}{*}[\symbolglyph{∅}]
                  & (imperatives & \C{cân~/ cana}     & \C{cenwch~/ canwch})
	\end{tabular}
\end{frame}


\begin{frame}{Number of occurrences}
	\begin{center}
		\begin{tabular}{lr}
			you & 1034\\
			your & 148\\
			yeh & 121\\
			yer & 60\\
			yourself & 14\\
			yours & 8\\
			yourselves & 5\\
			yerself & 2\\
			yerselves & 1\\
			\midrule
			& 1393
		\end{tabular}
	\end{center}
\end{frame}


\begin{frame}{Tagging examples}
	\begin{center}
		\includegraphics[width=\textwidth]{media/tagging.png}
	\end{center}
\end{frame}


\begin{frame}[shrink=15]{Mapping interpersonal relationships}
	\centering\small
	\begin{tabular}{lll}
		T-V & speaker & addressee\\
		\midrule
		\symbolglyph{⋮} & \symbolglyph{⋮} & \symbolglyph{⋮}\\
		\HLchisg{chi} & Doris Crockford & Harry      \\
		\HLti{ti}     & Draco           & Harry      \\
		\HLchisg{chi} & Draco           & McGonagall \\
		\HLti{ti}     & Draco           & Neville    \\
		\HLti{ti}     & Draco           & Ron        \\
		\HLti{ti}     & Dudley          & Harry      \\
		\HLti{ti}     & Dumbledore      & Hagrid     \\
		\HLti{ti}     & Dumbledore      & Harry      \\
		\HLti{ti}     & Dumbledore      & McGonagall \\
		\HLti{ti}     & Filch           & Harry      \\
		\HLti{ti}     & Filch           & Peeves     \\
		\HLchisg{chi} & Filch           & Snape      \\
		\HLti{ti}     & Firenze         & Bane       \\
		\HLchisg{chi} & Firenze         & Harry      \\
		\HLti{ti}     & Firenze         & Harry      \\
		\symbolglyph{⋮} & \symbolglyph{⋮} & \symbolglyph{⋮}
	\end{tabular}
\end{frame}
